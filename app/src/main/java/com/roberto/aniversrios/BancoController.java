package com.roberto.aniversrios;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class BancoController {
    private SQLiteDatabase db;
    private Banco banco;

    public BancoController(Context context){
        banco = new Banco(context);
    }

    public String inserirPessoa(Pessoa pessoa) {
        db = banco.getWritableDatabase();
        ContentValues valores = new ContentValues();
        valores.put("nome", pessoa.getNome());
        valores.put("dia", pessoa.getDia());
        valores.put("mes", pessoa.getMes());
        valores.put("ano", pessoa.getAno());

        long resultado = db.insert("pessoas", null, valores);
        db.close();

        if (resultado == -1)
            return "Erro ao inserir pessoa no banco de dados";

        return "Registrado com sucesso";
    }

    public ArrayList<Pessoa> buscarTodos() {
        ArrayList<Pessoa> pessoas = new ArrayList<>();
        db = banco.getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from pessoas",null);
        if (cursor.moveToFirst()) {
            do {
                String nome = cursor.getString(cursor.getColumnIndex("nome"));
                int dia = cursor.getInt(cursor.getColumnIndex("dia"));
                int mes = cursor.getInt(cursor.getColumnIndex("mes"));
                int ano = cursor.getInt(cursor.getColumnIndex("ano"));
                int id = cursor.getInt(cursor.getColumnIndex("id"));

                Pessoa pessoa = new Pessoa(nome, dia, mes, ano);
                pessoa.setId(id);
                pessoas.add(pessoa);
            } while (cursor.moveToNext());
        }

        db.close();
        return pessoas;
    }

    public void atualizar(Pessoa pessoa) {
        db = banco.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put("nome", pessoa.getNome());
        valores.put("dia", pessoa.getDia());
        valores.put("mes", pessoa.getMes());
        valores.put("ano", pessoa.getAno());

        db.update("pessoas", valores, "id=?", new String[] {pessoa.getId()+""});
        db.close();
    }

    public void deletar(Pessoa pessoa) {
        db = banco.getWritableDatabase();

        db.delete("pessoas", "id=?", new String[] {pessoa.getId()+""});
        db.close();
    }
}
