package com.roberto.aniversrios;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Banco extends SQLiteOpenHelper {
    private static final String NOME_BANCO = "bancodados.db";

    public Banco(Context contexto) {
        super(contexto, NOME_BANCO, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE pessoas ( "
                + "id integer primary key autoincrement, "
                + "nome text, "
                + "dia integer, "
                + "mes integer, "
                + "ano integer "
                + ")";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS pessoas");
        onCreate(db);
    }
}
