package com.roberto.aniversrios;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.Serializable;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    FloatingActionButton fabAdicionar;
    ListView listViewAniversarios;
    ArrayList<Pessoa> pessoas = new ArrayList<>();
    ArrayAdapter<Pessoa> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fabAdicionar = findViewById(R.id.fab_adicionar);
        fabAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AdicionarPessoa.class);
                Bundle bundle = new Bundle();
                bundle.putBoolean("editar", false);
                intent.putExtras(bundle);
                startActivityForResult(intent, 3);
            }
        });

        listViewAniversarios = findViewById(R.id.lista_nomes);
        BancoController banco = new BancoController(getBaseContext());
        pessoas = banco.buscarTodos();
        adapter = new ArrayAdapter<Pessoa>(this, android.R.layout.simple_list_item_1, pessoas);
        listViewAniversarios.setAdapter(adapter);

        listViewAniversarios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pessoa pessoa = pessoas.get(position);
                Intent intent = new Intent(MainActivity.this, AdicionarPessoa.class);
                Bundle bundle = new Bundle();
                bundle.putBoolean("editar", true);
                bundle.putSerializable("pessoa", pessoa);
                intent.putExtras(bundle);
                startActivityForResult(intent, 2);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        pessoas.clear();

        BancoController banco = new BancoController(getBaseContext());
        for (Pessoa pessoa : banco.buscarTodos()) {
            adapter.add(pessoa);
        }
    }
}
