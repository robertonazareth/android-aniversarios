package com.roberto.aniversrios;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AdicionarPessoa extends AppCompatActivity {
    Button buttonAdicionar;
    Button buttonDeletar;
    EditText editTextNome;
    TextView textViewDia;
    CalendarView calendarView;

    int dia = 1;
    int mes = 1;
    int ano = 2000;
    Boolean editar = false;
    Pessoa pessoa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adicionar_pessoa);

        buttonAdicionar = findViewById(R.id.button_adicionar);
        buttonDeletar = findViewById(R.id.button_deletar);
        editTextNome = findViewById(R.id.editText_nome);
        textViewDia = findViewById(R.id.textView_dia);
        calendarView = findViewById(R.id.calendarView);

        Bundle bundle = getIntent().getExtras();
        editar = bundle.getBoolean("editar");
        if (editar) {
            pessoa = (Pessoa)bundle.getSerializable("pessoa");
            dia = pessoa.getDia();
            mes = pessoa.getMes();
            ano = pessoa.getAno();

            String data = pessoa.getDia() + "/" + pessoa.getMes() + "/" + pessoa.getAno();
            try {
                calendarView.setDate(new SimpleDateFormat("dd/MM/yyyy").parse(data).getTime(), true, true);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            editTextNome.setText(pessoa.getNome());
            buttonAdicionar.setText("Editar");
            buttonDeletar.setVisibility(View.VISIBLE);
        }
        else {
            buttonAdicionar.setText("Adicionar");
            buttonDeletar.setVisibility(View.GONE);
        }

        buttonAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editar) {
                    pessoa.setNome(editTextNome.getText().toString());
                    pessoa.setDia(dia);
                    pessoa.setMes(mes);
                    pessoa.setAno(ano);

                    BancoController banco = new BancoController(getBaseContext());
                    banco.atualizar(pessoa);

                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
                }
                else {
                    String nome = editTextNome.getText().toString();
                    pessoa = new Pessoa(nome, dia, mes, ano);
                    BancoController banco = new BancoController(getBaseContext());

                    if (nome.isEmpty()) {
                        Snackbar.make(view,
                                "Erro, insira um nome",
                                Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();

                        return;
                    }

                    try {
                        banco.inserirPessoa(pessoa);
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        finish();
                    } catch (Exception e) {
                        Snackbar.make(view,
                                "Erro ao adicionar aniversário:\n" + e.getMessage(),
                                Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }
            }
        });

        buttonDeletar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BancoController banco = new BancoController(getBaseContext());
                banco.deletar(pessoa);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int ano, int mes, int dia) {
                AdicionarPessoa.this.ano = ano;
                AdicionarPessoa.this.mes = mes+1;
                AdicionarPessoa.this.dia = dia;
            }
        });
    }
}
